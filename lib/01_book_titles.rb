class Book
  SKIP_WORDS = [
    "the",
    "a",
    "an",
    "and",
    "in",
    "of"
  ]

  attr_reader :title

  def title=(title)
    words = title.downcase.split(' ')

    new_title = words.map.with_index do |word, i|
      if SKIP_WORDS.include?(word) && i != 0
        word
      else
        word.capitalize
      end
    end

    @title = new_title.join(' ')
  end
end
