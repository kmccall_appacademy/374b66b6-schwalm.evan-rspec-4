class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def format(num)
    if num > 10
      num.to_s
    else
      "0#{num}"
    end
  end

  def hours
    Integer(seconds / 3600)
  end

  def minutes
    Integer((seconds % 3600) / 60)
  end

  def re_seconds
    Integer((seconds % 60))
  end

  def time_string
    "#{format(hours)}:#{format(minutes)}:#{format(re_seconds)}"
  end
end
